import React from 'react';
import s from './formChat.module.css';

class FormChat extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            placeholder: 'Your message',
            value: ''
        };

    }


    handleChange = (event) => {

        this.setState({typeMessage: event.target.value});

    }


    render() {
        return (
            <div className={s.formWrap}>
                <form   className={s.sendMsgForm} onSubmit={(event) => this.props.addMessage(this.state.typeMessage, event)}>
                    <label className={s.sendLabel}>
                        <textarea className={s.input}
                                  value={this.state.typeMessage}
                                  onChange={this.handleChange}
                                  placeholder="Type your message and hit ENTER"
                            />
                    </label>
                    <input className={s.submitBtn} type="submit" value="Send" />
                </form>

            </div>
        )


    }
}

export default FormChat;

//onClick={(event) => this.props.addMessage(this.state.typeMessage, event)}