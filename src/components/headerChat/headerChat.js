import React from 'react';
import s from './headerChat.module.css';

class ChatHeader extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            chatTitle: 'My chat in React'
        }
    }


    render() {
        const {chatTitle} = this.state
        const {dataFromServer, numberMsg, users} = this.props.dataState;
        let lastElTime
        if (dataFromServer.length > 0) {
            lastElTime = dataFromServer[dataFromServer.length-1].time;
        }

        return (
            <div className={s.headerWrap}>
                <div className={s.mainInfo}>
                    <div className={s.title}>{chatTitle}</div>
                    <div className={s.participants}>{users.size} participants</div>
                    <div className={s.messages}>{numberMsg} messages</div>
                </div>
                <div className={s.dateInfo}>Last message at: {lastElTime}</div>
            </div>
        )


    }
}

export default ChatHeader ;