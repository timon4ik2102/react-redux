import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import Chat from './Chat';
import {Provider} from "react-redux";
// import { createStore } from 'redux'
// import rootReducer from './reducers'
import * as serviceWorker from './serviceWorker';

// const store = createStore(rootReducer)

ReactDOM.render(
    <React.StrictMode>
        <Provider>
            <Chat/>
        </Provider>
    </React.StrictMode>,
    document.getElementById('root')
);
serviceWorker.unregister();
